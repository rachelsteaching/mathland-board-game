console.log( "game.js" );

gameState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    background: null,
    player1Selection : 0,
    player2Selection : 0,
    player3Selection : 0,
    turn: 1,
    gameOver: false,
    winner: 0,
    glow: null,
    phase: "spin",
    
    characters: {   "red":[ "red-cat", "red-dino", "red-guy" ],
                    "green": [ "green-cat", "green-dino", "green-guy" ],
                    "blue": [ "blue-cat", "blue-dino", "blue-guy" ]
                },

    boardSpots: [
        { x : 130, y : 600, type : "" },
        { x : 130, y : 500, type : "add" },
        { x : 140, y : 350, type : "sub" },
        { x : 300, y : 350, type : "mult" },
        { x : 300, y : 250, type : "add" },
        { x : 475, y : 250, type : "add" },
        { x : 475, y : 150, type : "sub" },
        { x : 475, y : 25, type : "mult" },
        { x : 650, y : 25, type : "sub" },
        { x : 810, y : 25, type : "add" },
        { x : 810, y : 150, type : "sub" },
        { x : 810, y : 250, type : "mult" },
        { x : 810, y : 350, type : "mult" },
        { x : 810, y : 500, type : "add" },
        { x : 975, y : 500, type : "add" },
        { x : 1150, y : 500, type : "sub" },
        { x : 1150, y : 350, type : "mult" },
        { x : 1150, y : 250, type : "add" },
        { x : 1150, y : 150, type : "sub" },
        { x : 1000, y : 25, type : "add" },
    ],
    
    keys: {
        UP:         { code: "w", isDown: false }, 
        DOWN:       { code: "s", isDown: false },
        RIGHT:      { code: "d", isDown: false },
        LEFT:       { code: "a", isDown: false },
        SHOOT:      { code: 32, isDown: false },
    },

    player1: {
        x: 65, y: 600,
        boardSpot: 0,
        width: 58, height: 88,
        fullWidth: 230, fullHeight: 350,
        image: null,
        points: 0,

        Move: function( spaces ) {
            var wasCorrect = gameState.MathQuestion( this.boardSpot + spaces );

            if ( wasCorrect )
                this.Goto( this.boardSpot + spaces );
            
            gameState.NextTurn();
        },

        Goto: function( spot ) {
            if ( spot >= gameState.boardSpots.length )
            {
                spot = gameState.boardSpots.length - 1;
            }
            this.x = gameState.boardSpots[spot]["x"] - 50;
            this.y = gameState.boardSpots[spot]["y"];
            this.boardSpot = spot;
        }
    },

    player2: {
        x: 165, y: 600,
        boardSpot: 0,
        width: 58, height: 88,
        fullWidth: 230, fullHeight: 350,
        image: null,
        points: 0,

        Move: function( spaces ) {
            var wasCorrect = gameState.MathQuestion( this.boardSpot + spaces );

            if ( wasCorrect )
                this.Goto( this.boardSpot + spaces );
            
            gameState.NextTurn();
        },

        Goto: function( spot ) {
            if ( spot >= gameState.boardSpots.length )
            {
                spot = gameState.boardSpots.length - 1;
            }
            this.x = gameState.boardSpots[spot]["x"];
            this.y = gameState.boardSpots[spot]["y"];
            this.boardSpot = spot;
        }
    },

    player3: {
        x: 265, y: 600,
        boardSpot: 0,
        width: 58, height: 88,
        fullWidth: 230, fullHeight: 350,
        image: null,
        points: 0,

        Move: function( spaces ) {
            var wasCorrect = gameState.MathQuestion( this.boardSpot + spaces );

            if ( wasCorrect )
                this.Goto( this.boardSpot + spaces );
            
            gameState.NextTurn();
        },

        Goto: function( spot ) {
            if ( spot >= gameState.boardSpots.length )
            {
                spot = gameState.boardSpots.length - 1;
            }
            this.x = gameState.boardSpots[spot]["x"] + 50;
            this.y = gameState.boardSpots[spot]["y"];
            this.boardSpot = spot;
        }
    },

    spinner: {
        number : 0,
        x : 0, y : 0,
        width: 228, height: 228,
        fullWidth: 228, fullHeight: 228,
        image : null,
        spinCountdown: 0,

        BeginSpin: function() {
            this.spinCountdown = 20;
            var value = Math.floor( Math.random() * 4 ) + 1;
            this.number = value;
            console.log( this.number );
            gameState.phase = "move";
        },

        Update: function() {
            if ( this.spinCountdown > 0 )
            {
                this.Spin();
                this.spinCountdown -= 1;
                ASSET_TOOLS.PlaySound( "spin" );
                
                if ( this.spinCountdown == 0 )
                {
                    UI_TOOLS.UpdateText( "info", "Click Player " + gameState.turn + "! Answer a question to move!" );
                }
            }
        },

        Spin: function() {
            this.number++;
            if ( this.number > 4 )
                this.number = 1;
        },
    },

    Init: function( canvas, options ) {
        gameState.background = new Image();
        gameState.background.src = "assets/images/board.png";
        
        gameState.canvas = canvas;
        gameState.options = options;
        gameState.isDone = false;

        ASSET_TOOLS.AddSound( { "title" : "spin", "file" : "assets/audio/spin.wav", "loop" : false, "isMusic" : false, "volume" : 1 } );
        ASSET_TOOLS.AddSound( { "title" : "right", "file" : "assets/audio/correct.wav", "loop" : false, "isMusic" : false, "volume" : 1 } );
        ASSET_TOOLS.AddSound( { "title" : "wrong", "file" : "assets/audio/wrong.wav", "loop" : false, "isMusic" : false, "volume" : 1 } );

        gameState.player1.image = new Image();
        gameState.player2.image = new Image();
        gameState.player3.image = new Image();
        gameState.spinner.image = new Image();
        
        gameState.player1.image.src = "assets/images/" + gameState.characters["red"][titleState.player1Selection] + ".png";
        gameState.player2.image.src = "assets/images/" + gameState.characters["green"][titleState.player2Selection] + ".png";
        gameState.player3.image.src = "assets/images/" + gameState.characters["blue"][titleState.player3Selection] + ".png";
        gameState.spinner.image.src = "assets/images/spinner.png";

        gameState.player1.Goto( 0 );
        gameState.player2.Goto( 0 );
        gameState.player3.Goto( 0 );

        gameState.glow = new Image();
        gameState.glow.src = "assets/images/glow.png";

        UI_TOOLS.CreateText( { "title" : "info", "words" : "Player 1's turn! Click the spinner!", "x" : 400, "y" : 685, "font" : "30px Fredoka One", "color" : "#ffffff" } );
        
        UI_TOOLS.CreateText( { "title" : "points1", "words" : "Player 1: 0", "x" : 500, "y" : 450, "font" : "30px Fredoka One", "color" : "#ffffff" } );
        UI_TOOLS.CreateText( { "title" : "points2", "words" : "Player 2: 0", "x" : 500, "y" : 500, "font" : "30px Fredoka One", "color" : "#ffffff" } );
        UI_TOOLS.CreateText( { "title" : "points3", "words" : "Player 3: 0", "x" : 500, "y" : 550, "font" : "30px Fredoka One", "color" : "#ffffff" } );
        
        UI_TOOLS.CreateText( { "title" : "win", "words" : "", "x" : 1280/4, "y" : 720/2, "font" : "100px Fredoka One", "color" : "#ffffff" } );
    },

    MathQuestion: function( spot ) {
        var num2 = Math.floor( Math.random() * 12 ) + 1;
        var num1 = Math.floor( Math.random() * 12 ) + num2;
        var answer;

        var str = "What is " + num1;
        if ( gameState.boardSpots[ spot ][ "type" ] == "add" )
        {
            answer = num1 + num2;
            str += " plus ";
        }
        else if ( gameState.boardSpots[ spot ][ "type" ] == "sub" )
        {
            answer = num1 - num2;
            str += " minus ";
        }
        else if ( gameState.boardSpots[ spot ][ "type" ] == "mult" )
        {
            answer = num1 * num2;
            str += " times ";
        }
        else if ( gameState.boardSpots[ spot ][ "type" ] == "div" )
        {
            answer = num1 / num2;
            str += " divided by ";
        }
        
        str += num2 + "?";

        var playerAnswer = window.prompt( str, "" );

        var correct = true;
        
        if ( playerAnswer == answer )
        {
            ASSET_TOOLS.PlaySound( "right" );
            alert( "Right!" );
            if ( gameState.turn == 1 )      { gameState.player1.points += 1; }
            else if ( gameState.turn == 2 ) { gameState.player2.points += 1; }
            else if ( gameState.turn == 3 ) { gameState.player3.points += 1; }
        }
        else
        {
            ASSET_TOOLS.PlaySound( "wrong" );
            alert( "Wrong!" );
            correct = false;
        }

        UI_TOOLS.UpdateText( "points1", "Player 1: " + gameState.player1.points );
        UI_TOOLS.UpdateText( "points2", "Player 2: " + gameState.player2.points );
        UI_TOOLS.UpdateText( "points3", "Player 3: " + gameState.player3.points );

        return correct;
    },

    NextTurn: function() {
        gameState.spinner.number = 0;
        
        gameState.turn = gameState.turn + 1;
        if ( gameState.turn == 4 )
            gameState.turn = 1;

        gameState.phase = "spin";

        UI_TOOLS.UpdateText( "info", "Player " + gameState.turn + "'s turn! Click the spinner!" );
    },

    Clear: function() {
    },

    Click: function( ev ) {
        if ( gameState.gameOver )
        {
            return;
        }
        
        var windowRect = $( "canvas" )[0].getBoundingClientRect();
        var mouseX = event.clientX - windowRect.left;
        var mouseY = event.clientY - windowRect.top;
        
        if ( gameState.IsClicked( mouseX, mouseY, gameState.spinner ) && gameState.phase == "spin" )
        {
            gameState.spinner.BeginSpin();
        }

        if ( gameState.IsClicked( mouseX, mouseY, gameState.player1 ) && gameState.turn == 1 && gameState.phase == "move" )
            gameState.player1.Move( gameState.spinner.number );
            
        if ( gameState.IsClicked( mouseX, mouseY, gameState.player2 ) && gameState.turn == 2 && gameState.phase == "move"  )
            gameState.player2.Move( gameState.spinner.number );
            
        if ( gameState.IsClicked( mouseX, mouseY, gameState.player3 ) && gameState.turn == 3 && gameState.phase == "move"  )
            gameState.player3.Move( gameState.spinner.number );
    },

    IsClicked: function( mouseX, mouseY, obj ) {
        var objX = obj.x;
        var objY = obj.y;
        var objW = obj.width;
        var objH = obj.height;
        return ( mouseX >= objX && mouseX <= objX + objW && mouseY >= objY && mouseY <= objY + objH );
    },

    KeyPress: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = true;
            }
        } );
    },

    KeyRelease: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = false;
            }
        } );
    },

    Update: function() {
        if ( gameState.keys.UP.isDown ) {
            gameState.objBear.Move( "UP" );
        }
        else if ( gameState.keys.DOWN.isDown ) {
            gameState.objBear.Move( "DOWN" );
        }
        if ( gameState.keys.LEFT.isDown ) {
            gameState.objBear.Move( "LEFT" );
        }
        else if ( gameState.keys.RIGHT.isDown ) {
            gameState.objBear.Move( "RIGHT" );
        }

        gameState.spinner.Update();

        if ( gameState.player1.boardSpot == gameState.boardSpots.length - 1 )
        {
            gameState.gameOver = true;
            gameState.winner = 1;
            UI_TOOLS.UpdateText( "win", "Player " + gameState.winner + " wins!!" );
        }
        
        if ( gameState.player2.boardSpot == gameState.boardSpots.length - 1 )
        {
            gameState.gameOver = true;
            gameState.winner = 2;
            UI_TOOLS.UpdateText( "win", "Player " + gameState.winner + " wins!!" );
        }
        
        if ( gameState.player3.boardSpot == gameState.boardSpots.length - 1 )
        {
            gameState.gameOver = true;
            gameState.winner = 3;
            UI_TOOLS.UpdateText( "win", "Player " + gameState.winner + " wins!!" );
        }
    },

    Draw: function() {
        // Draw grass 9cc978
        main.canvasWindow.fillStyle = "#9cc978";
        main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );
        main.canvasWindow.drawImage( gameState.background, 0, 0 );

        var glowX, glowY, glowW, glowH;

        if ( gameState.turn == 1 )
        {
            glowX = gameState.player1.x;
            glowY = gameState.player1.y - 20;
            glowW = gameState.player1.width;
            glowH = gameState.player1.height + 20;
        }
        else if ( gameState.turn == 2 )
        {
            glowX = gameState.player2.x;
            glowY = gameState.player2.y - 20;
            glowW = gameState.player2.width;
            glowH = gameState.player2.height + 20;
        }
        else if ( gameState.turn == 3 )
        {
            glowX = gameState.player3.x;
            glowY = gameState.player3.y - 20;
            glowW = gameState.player3.width;
            glowH = gameState.player3.height + 20;
        }
        
        main.canvasWindow.drawImage( gameState.glow, glowX, glowY, glowW, glowH );

        // context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
        main.canvasWindow.drawImage( gameState.spinner.image,
            gameState.spinner.number * gameState.spinner.width, 0,          // clipping
            gameState.spinner.width,        gameState.spinner.height,       // clipping
            gameState.spinner.x,            gameState.spinner.y,            // position
            gameState.spinner.fullWidth,    gameState.spinner.fullHeight );
            
        main.canvasWindow.drawImage( gameState.player1.image,
            gameState.player1.x,            gameState.player1.y,
            gameState.player1.width,        gameState.player1.height );

        main.canvasWindow.drawImage( gameState.player2.image,
            gameState.player2.x,            gameState.player2.y,
            gameState.player2.width,        gameState.player2.height );

        main.canvasWindow.drawImage( gameState.player3.image,
            gameState.player3.x,            gameState.player3.y,
            gameState.player3.width,        gameState.player3.height );

        UI_TOOLS.Draw( main.canvasWindow );
    },

    ClickPlay: function() {
    }
};
