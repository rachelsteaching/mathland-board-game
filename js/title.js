console.log( "title.js" );

titleState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    characters: {   "red":[ "red-cat", "red-dino", "red-guy" ],
                    "green": [ "green-cat", "green-dino", "green-guy" ],
                    "blue": [ "blue-cat", "blue-dino", "blue-guy" ]
                },

    player1Selection: 0,
    player2Selection: 1,
    player3Selection: 2,

    Init: function( canvas, options ) {
        titleState.canvas = canvas;
        titleState.options = options;
        titleState.isDone = false;

        //ASSET_TOOLS.AddSound( { title: "title",
                            //file: "assets/audio/DancingBunnies_Moosader.ogg",
                            //volume: 0.1,
                            //loop: true,
                            //isMusic: true
                            //} );

        UI_TOOLS.CreateImage( { title: "background",
                            src:    "assets/images/title.png",
                            x: 0, y: 0,
                            width: 1280, height: 720,
                            fullWidth: options.width, fullHeight: options.height } );
                            
        UI_TOOLS.CreateText( { title: "Title", words: LANGUAGE_TOOLS.GetText( "English", "title" ),
                            color: "#ff5400", font: "100px Fredoka One", x: options.width/2 - 250, y: 100 } );

        //UI_TOOLS.CreateText( { title: "Version", words: "v1.0", color: "#000000", font: "bold 16px Sans-serif", x: 450, y: 200 } );

        var text = "by Rachel Wil Sha Singh";
        UI_TOOLS.CreateText( { title: "by", words: text,
                             color: "#000000", font: "bold 16px Courier",
                             x: ( titleState.options.width / 2 ) - ( text.length * 9 ) / 2,
                             y: options.height - 10 } );

        UI_TOOLS.CreateButton( { title: "helpButton", words: LANGUAGE_TOOLS.GetText( "English", "help" ),
                             color: "#c02848", font: "35px Fredoka One",
                             src: "assets/images/button.png",
                             x: 10, y: options.height - 100,
                             textX: 25, textY: 50,
                             width: 200, height: 100,
                             fullWidth: 125, fullHeight: 75,
                             soundType : "confirm",
                             Click: function() {
                                 main.changeState( "helpState" );
                                 } } );
                                 
        UI_TOOLS.CreateButton( { title: "playButton", words: LANGUAGE_TOOLS.GetText( "English", "play" ),
                             color: "#c02848", font: "35px Fredoka One",
                             src: "assets/images/button.png",
                             x: 1140, y: options.height - 100,
                             textX: 20, textY: 50,
                             width: 200, height: 100,
                             fullWidth: 125, fullHeight: 75,
                             soundType : "confirm",
                             Click: function() {
                                 main.changeState( "gameState" );
                                 } } );
                         
        UI_TOOLS.CreateText( { title: "p1box", words: "Player 1", color: "#000000", font: "40px Fredoka One", x: 200, y: 175 } ); 
        UI_TOOLS.CreateImage( { title: "p1",
                            src:    "assets/images/" + titleState.characters["red"][titleState.player1Selection] + ".png",
                            x: 160, y: 175, width: 230, height: 350, fullWidth: 230, fullHeight: 350 } );       
        UI_TOOLS.CreateButton( { title: "p1_prev", src: "assets/images/btn-prev.png",
                             soundType : "confirm",
                             x: 142, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                 titleState.player1Selection--;
                                 if ( titleState.player1Selection == -1 )
                                    titleState.player1Selection = 2;  
                                 titleState.UpdateCharacterPortraits();                               
                                 } } );
        UI_TOOLS.CreateButton( { title: "p1_next", src: "assets/images/btn-next.png",
                             soundType : "confirm",
                             x: 350, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                 titleState.player1Selection++;
                                 if ( titleState.player1Selection == 3 )
                                    titleState.player1Selection = 0; 
                                 titleState.UpdateCharacterPortraits();                                  
                                 } } );
                                 
                            

        UI_TOOLS.CreateText( { title: "p2box", words: "Player 2", color: "#000000", font: "40px Fredoka One", x: 560, y: 175 } );
        UI_TOOLS.CreateImage( { title: "p2",
                            src:    "assets/images/" + titleState.characters["green"][titleState.player2Selection] + ".png",
                            x: 530, y: 175, width: 230, height: 350, fullWidth: 230, fullHeight: 350 } );   
        UI_TOOLS.CreateButton( { title: "p2_prev", src: "assets/images/btn-prev.png",
                             soundType : "confirm",
                             x: 510, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                 titleState.player2Selection--;
                                 if ( titleState.player2Selection == -1 )
                                    titleState.player2Selection = 2; 
                                 titleState.UpdateCharacterPortraits();                                
                                 } } );
                                 
        UI_TOOLS.CreateButton( { title: "p2_next", src: "assets/images/btn-next.png",
                             soundType : "confirm",
                             x: 718, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                 titleState.player2Selection++;
                                 if ( titleState.player2Selection == 3 )
                                    titleState.player2Selection = 0; 
                                 titleState.UpdateCharacterPortraits();                                
                                 } } );
                                 
        UI_TOOLS.CreateText( { title: "p3box", words: "Player 3", color: "#000000", font: "40px Fredoka One", x: 935, y: 175 } );    
        UI_TOOLS.CreateImage( { title: "p3",
                            src:    "assets/images/" + titleState.characters["blue"][titleState.player3Selection] + ".png",
                            x: 898, y: 175, width: 230, height: 350, fullWidth: 230, fullHeight: 350 } );
        UI_TOOLS.CreateButton( { title: "p3_prev", src: "assets/images/btn-prev.png",
                             soundType : "confirm",
                             x: 878, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                 titleState.player3Selection--;
                                 if ( titleState.player3Selection == -1 )
                                    titleState.player3Selection = 2;
                                 titleState.UpdateCharacterPortraits();
                                 
                                 } } );
        UI_TOOLS.CreateButton( { title: "p3_next", src: "assets/images/btn-next.png",
                             soundType : "confirm",
                             x: 1086, y: 510, width: 51, height: 72, fullWidth: 51, fullHeight: 72,
                             Click: function() {
                                titleState.player3Selection++;
                                if ( titleState.player3Selection == 3 )
                                    titleState.player3Selection = 0;
                                titleState.UpdateCharacterPortraits();
                                 } } );


        //SONO_ILO.LudiMuzikon( "menuoMuziko" );
        //ASSET_TOOLS.PlayMusic( "title" );
    },

    UpdateCharacterPortraits() {
        UI_TOOLS.UpdateImage( { title : "p1", src : "assets/images/" + titleState.characters["red"][titleState.player1Selection] + ".png" } );
        UI_TOOLS.UpdateImage( { title : "p2", src : "assets/images/" + titleState.characters["green"][titleState.player2Selection] + ".png" } );
        UI_TOOLS.UpdateImage( { title : "p3", src : "assets/images/" + titleState.characters["blue"][titleState.player3Selection] + ".png" } );
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
    },

    KeyRelease: function( ev ) {
    },

    Update: function() {
    },

    Draw: function() {
        UI_TOOLS.Draw( titleState.canvas );
    },
};
